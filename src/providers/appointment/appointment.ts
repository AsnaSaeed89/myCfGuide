import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the AppointmentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class AppointmentProvider {
  http: any;
  apiKey: any;
  url: any;

  constructor(public httpClient: HttpClient) {
    console.log('Hello DietplanProvider Provider');
    this.http = httpClient;
    this.apiKey = 'mCHR83DlH4wfabk39rEezJkB8fP0rWv4';
    this.url = 'https://api.mlab.com/api/1/databases/mycfguide/collections/appointmentInfo';
  }

  fetchData() {
    return this.http.get(this.url + '?apiKey=' + this.apiKey);
  }

  saveData(object) {
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      
      return this.http.post(this.url + '?apiKey=' + this.apiKey, object, { headers: headers });
  }

  removeData(id) {
      return this.http.delete(this.url + '/' + id + '?apiKey=' + this.apiKey);
  }

}
