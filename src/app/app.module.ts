import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { MealDetailPage } from '../pages/meal-detail/meal-detail';
import { MealListPage } from '../pages/meal-list/meal-list'
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import { MedicationAddPage } from '../pages/medication-add/medication-add';
import { MedicationListPage } from '../pages/medication-list/medication-list';
import { MedicationProvider } from '../providers/medication/medication';

import { ApppointmentAddPage } from '../pages/appointment-add/appointment-add';
import { AppointmentListPage } from '../pages/appointment-list/appointment-list';
import { AppointmentProvider } from '../providers/appointment/appointment';
import { BreakfastListPage } from '../pages/breakfast-list/breakfast-list';
import { OmlettePage } from '../pages/breakfast-recipie/omlette/omlette';
import { GranolaPage } from '../pages/breakfast-recipie/granola/granola';
import { ToastPage } from '../pages/breakfast-recipie/toast/toast';
import { BeanToastPage } from '../pages/breakfast-recipie/bean-toast/bean-toast';
import { LunchListPage } from '../pages/lunch-list/lunch-list';
import { DinnerListPage } from '../pages/dinner-list/dinner-list';
import { DessertListPage } from '../pages/dessert-list/dessert-list';
import { SnackListPage } from '../pages/snack-list/snack-list';
import { SmoothieListPage } from '../pages/smoothie-list/smoothie-list';
import { ProfilePage } from '../pages/profile/profile';
import { AsianSaladPage } from '../pages/lunch-recipe/asian-salad/asian-salad';
import { ButterSoupPage } from '../pages/lunch-recipe/butter-soup/butter-soup';
import { CheeceLasagnePage } from  '../pages/lunch-recipe/cheece-lasagne/cheece-lasagne';
import { SalminHerbPage } from  '../pages/lunch-recipe/salmin-herb/salmin-herb';
import { ChickenCasserolePage } from '../pages//dinner-recipe/chicken-casserole/chicken-casserole';
import { MeatCakePage } from '../pages/dinner-recipe/meat-cake/meat-cake';
import { PotatoCasserolePage } from '../pages/dinner-recipe/potato-casserole/potato-casserole';
import { TurkeySupremePage } from '../pages/dinner-recipe/turkey-supreme/turkey-supreme';
import { FantasyFudgePage } from '../pages/dessert-recipe/fantasy-fudge/fantasy-fudge';
import { CherryDhanishPage } from '../pages/dessert-recipe/cherry-dhanish/cherry-dhanish';
import { IceCreamBreadPage } from '../pages/dessert-recipe/ice-cream-bread/ice-cream-bread';
import { OreoBallPage } from '../pages/dessert-recipe/oreo-ball/oreo-ball';
import { CornPuddingPage } from '../pages/snack-recipe/corn-pudding/corn-pudding';
import { ExtraPuddingPage } from '../pages/snack-recipe/extra-pudding/extra-pudding';
import { GarlicPopCornPage } from '../pages/snack-recipe/garlic-pop-corn/garlic-pop-corn';
import { MeatballPage } from '../pages/snack-recipe/meatball/meatball';
import { HighCalorieSmoothiePage } from '../pages/smoothie-recipe/high-calorie-smoothie/high-calorie-smoothie';
import { HighProtienSmoothiePage } from '../pages/smoothie-recipe/high-protien-smoothie/high-protien-smoothie';
import { PurpleSmoothiePage } from '../pages/smoothie-recipe/purple-smoothie/purple-smoothie';
import { TropicalSmoothiePage } from '../pages/smoothie-recipe/tropical-smoothie/tropical-smoothie';

@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    ApppointmentAddPage,
    AppointmentListPage,
    SessionDetailPage,
    SignupPage,
    MealDetailPage,
    MealListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    MedicationListPage,
    MedicationAddPage,
    BreakfastListPage,
    OmlettePage,
    GranolaPage,
    ToastPage,
    BeanToastPage,
    LunchListPage,
    DinnerListPage,
    SnackListPage,
    DessertListPage,
    SmoothieListPage,
    ProfilePage,
    AsianSaladPage,
    ButterSoupPage,
    CheeceLasagnePage,
    SalminHerbPage,
    ChickenCasserolePage,
    MeatCakePage,
    PotatoCasserolePage,
    TurkeySupremePage,
    FantasyFudgePage,
    CherryDhanishPage,
    IceCreamBreadPage,
    OreoBallPage,
    CornPuddingPage,
    ExtraPuddingPage,
    GarlicPopCornPage,
    MeatballPage,
    HighCalorieSmoothiePage,
    HighProtienSmoothiePage,
    PurpleSmoothiePage,
    TropicalSmoothiePage

    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
        { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
        { component: MealListPage, name: 'MealList', segment: 'mealList' },
        { component: MealDetailPage, name: 'MealDetail', segment: 'mealDetail/:MealId' },
        { component: MapPage, name: 'Map', segment: 'map' },
        { component: AboutPage, name: 'About', segment: 'about' },
        { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
        { component: SupportPage, name: 'SupportPage', segment: 'support' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
        { component: SignupPage, name: 'SignupPage', segment: 'signup' }
      ]
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    ApppointmentAddPage,
    AppointmentListPage,
    SessionDetailPage,
    SignupPage,
    MealDetailPage,
    MealListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    MedicationListPage,
    MedicationAddPage,
    BreakfastListPage,
    OmlettePage,
    GranolaPage,
    ToastPage,
    BeanToastPage,
    LunchListPage,
    DinnerListPage,
    SnackListPage,
    DessertListPage,
    SmoothieListPage,
    ProfilePage,
    AsianSaladPage,
    ButterSoupPage,
    CheeceLasagnePage,
    SalminHerbPage,
    ChickenCasserolePage,
    MeatCakePage,
    PotatoCasserolePage,
    TurkeySupremePage,
    FantasyFudgePage,
    CherryDhanishPage,
    IceCreamBreadPage,
    OreoBallPage,
    CornPuddingPage,
    ExtraPuddingPage,
    GarlicPopCornPage,
    MeatballPage,
    HighCalorieSmoothiePage,
    HighProtienSmoothiePage,
    PurpleSmoothiePage,
    TropicalSmoothiePage


  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen,
    MedicationProvider,
    AppointmentProvider
  ]
})
export class AppModule { }
