import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MedicationAddPage } from '../medication-add/medication-add';
import { MedicationProvider } from '../../providers/medication/medication';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the MedicationListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-medication-list',
  templateUrl: 'medication-list.html',
})
export class MedicationListPage {
  list:Observable<any>;

  constructor(private medprovider: MedicationProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.list = this.medprovider.fetchData();
      
    this.list
    .subscribe(data => {
      console.log('result: ', data);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicationListPage');
  }

  openMedAdd(){
    this.navCtrl.push(MedicationAddPage, {
      item: "New medicine"
    });
    return false;
  }

}
