import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DinnerListPage } from './dinner-list';

@NgModule({
  declarations: [
    DinnerListPage,
  ],
  imports: [
    IonicPageModule.forChild(DinnerListPage),
  ],
})
export class DinnerListPageModule {}
