import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChickenCasserolePage } from '../dinner-recipe/chicken-casserole/chicken-casserole';
import { MeatCakePage } from '../dinner-recipe/meat-cake/meat-cake';
import { PotatoCasserolePage } from '../dinner-recipe/potato-casserole/potato-casserole';
import { TurkeySupremePage } from '../dinner-recipe/turkey-supreme/turkey-supreme';
/**
 * Generated class for the DinnerListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dinner-list',
  templateUrl: 'dinner-list.html',
})
export class DinnerListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DinnerListPage');
  }

  goToCasserole(){
    this.navCtrl.push(ChickenCasserolePage);
  }

  goToCurriedCasserole(){
    this.navCtrl.push(PotatoCasserolePage);
  }
  goToMeakCake(){
    this.navCtrl.push(MeatCakePage);
  }

  goToSupreme(){
    this.navCtrl.push(TurkeySupremePage);
  }
}
