import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { AppointmentListPage } from '../appointment-list/appointment-list';

/**
 * Generated class for the AppointmentAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-appointment-add',
  templateUrl: 'appointment-add.html',
})
export class ApppointmentAddPage {

  myreason: string;
  mycenter: string;
  myday:string;
  mytime:string;

  appointment:Observable<any>;


  constructor(private appointmentprovider: AppointmentProvider,public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApppointmentAddPage');
  }

  
  appointmentAdd(){

    this.appointment = this.appointmentprovider.saveData({reason:this.myreason,center:this.mycenter,appdate:this.myday,time:this.mytime});
        this.appointment
        .subscribe(data => {
              console.log('result: ', data);
              this.navCtrl.push(AppointmentListPage, {
                item: "New Appointment"
              });
              return false;
        })
  }
}
