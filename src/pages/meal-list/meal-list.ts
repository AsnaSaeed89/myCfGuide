import { Component } from '@angular/core';
import { BreakfastListPage } from '../breakfast-list/breakfast-list';
import { LunchListPage } from '../lunch-list/lunch-list';
import { DinnerListPage } from '../dinner-list/dinner-list';
import { DessertListPage } from '../dessert-list/dessert-list';
import { SnackListPage } from '../snack-list/snack-list';
import { SmoothieListPage } from '../smoothie-list/smoothie-list';
import { NavController, NavParams } from 'ionic-angular';






@Component({
  selector: 'page-meal-list',
  templateUrl: 'meal-list.html',
  entryComponents: [BreakfastListPage]
})
export class MealListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams)
  {
  }
  
  ionViewDidLoad() {
  console.log('ionViewDidLoad WelcomePage');
  }


  
  gotoBreakfast(){
    this.navCtrl.push(BreakfastListPage, {
      item: "Breakfast List"
    });
    return false;
  }

  gotoLunch(){
    this.navCtrl.push(LunchListPage, {
      item: "Lunch List"
    });
    return false;
  }

  gotoDinner(){
    this.navCtrl.push(DinnerListPage, {
      item: "Breakfast List"
    });
    return false;
  }

  gotoSnack(){
    this.navCtrl.push(SnackListPage, {
      item: "Breakfast List"
    });
    return false;
  }

  gotoDessert(){
    this.navCtrl.push(DessertListPage, {
      item: "Breakfast List"
    });
    return false;
  }

  gotoSmoothie(){
    this.navCtrl.push(SmoothieListPage, {
      item: "Breakfast List"
    });
    return false;
  }
}