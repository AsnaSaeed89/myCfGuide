import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalminHerbPage } from './salmin-herb';

@NgModule({
  declarations: [
    SalminHerbPage,
  ],
  imports: [
    IonicPageModule.forChild(SalminHerbPage),
  ],
})
export class SalminHerbPageModule {}
