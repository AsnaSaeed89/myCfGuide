import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsianSaladPage } from './asian-salad';

@NgModule({
  declarations: [
    AsianSaladPage,
  ],
  imports: [
    IonicPageModule.forChild(AsianSaladPage),
  ],
})
export class AsianSaladPageModule {}
