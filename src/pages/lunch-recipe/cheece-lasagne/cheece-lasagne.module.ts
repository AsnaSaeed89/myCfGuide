import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheeceLasagnePage } from './cheece-lasagne';

@NgModule({
  declarations: [
    CheeceLasagnePage,
  ],
  imports: [
    IonicPageModule.forChild(CheeceLasagnePage),
  ],
})
export class CheeceLasagnePageModule {}
