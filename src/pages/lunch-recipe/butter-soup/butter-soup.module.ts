import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButterSoupPage } from './butter-soup';

@NgModule({
  declarations: [
    ButterSoupPage,
  ],
  imports: [
    IonicPageModule.forChild(ButterSoupPage),
  ],
})
export class ButterSoupPageModule {}
