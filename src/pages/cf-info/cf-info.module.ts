import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CfInfoPage } from './cf-info';

@NgModule({
  declarations: [
    CfInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(CfInfoPage),
  ],
})
export class CfInfoPageModule {}
