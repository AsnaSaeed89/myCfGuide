import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GarlicPopCornPage } from './garlic-pop-corn';

@NgModule({
  declarations: [
    GarlicPopCornPage,
  ],
  imports: [
    IonicPageModule.forChild(GarlicPopCornPage),
  ],
})
export class GarlicPopCornPageModule {}
