import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeatballPage } from './meatball';

@NgModule({
  declarations: [
    MeatballPage,
  ],
  imports: [
    IonicPageModule.forChild(MeatballPage),
  ],
})
export class MeatballPageModule {}
