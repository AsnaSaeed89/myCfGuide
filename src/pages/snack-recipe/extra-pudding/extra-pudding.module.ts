import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExtraPuddingPage } from './extra-pudding';

@NgModule({
  declarations: [
    ExtraPuddingPage,
  ],
  imports: [
    IonicPageModule.forChild(ExtraPuddingPage),
  ],
})
export class ExtraPuddingPageModule {}
