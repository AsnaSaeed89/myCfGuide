import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CornPuddingPage } from './corn-pudding';

@NgModule({
  declarations: [
    CornPuddingPage,
  ],
  imports: [
    IonicPageModule.forChild(CornPuddingPage),
  ],
})
export class CornPuddingPageModule {}
