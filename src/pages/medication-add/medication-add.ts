import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { MedicationProvider } from '../../providers/medication/medication';
import { MedicationListPage } from '../medication-list/medication-list';

/**
 * Generated class for the MedicationAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-medication-add',
  templateUrl: 'medication-add.html',
})
export class MedicationAddPage {

  
  mymedicine: string;
  myday:string;
  mytime:string;

  med:Observable<any>;


  constructor(private medprovider: MedicationProvider,public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicationAddPage');
  }

  
  medicineAdd(){

    this.med = this.medprovider.saveData({medname:this.mymedicine,medday:this.myday,medtime:this.mytime});
        this.med
        .subscribe(data => {
              console.log('result: ', data);
              this.navCtrl.push(MedicationListPage, {
                item: "New medicine"
              });
              return false;
        })
  }
}
