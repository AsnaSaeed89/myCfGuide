import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedDeletePage } from './med-delete';

@NgModule({
  declarations: [
    MedDeletePage,
  ],
  imports: [
    IonicPageModule.forChild(MedDeletePage),
  ],
})
export class MedDeletePageModule {}
