import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { MapPage } from '../map/map';
import { AppointmentListPage } from '../appointment-list/appointment-list';
import { MealListPage } from '../meal-list/meal-list';
import { MedicationAddPage } from '../medication-add/medication-add';
import { MedicationListPage } from '../medication-list/medication-list';
import { ProfilePage } from '../profile/profile';

@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = AppointmentListPage;
  tab2Root: any = MealListPage;
  tab3Root: any = MapPage;
  tab4Root: any = AboutPage;
  tab5Root: any = MedicationAddPage;   //add new pages : ionic g page <pagename> --no-module
  tab6Root: any = MedicationListPage;
  tab7Root: any = ProfilePage    
  
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
