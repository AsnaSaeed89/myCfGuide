import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CornPuddingPage } from '../snack-recipe/corn-pudding/corn-pudding';
import { ExtraPuddingPage } from '../snack-recipe/extra-pudding/extra-pudding';
import { GarlicPopCornPage } from '../snack-recipe/garlic-pop-corn/garlic-pop-corn';
import { MeatballPage } from '../snack-recipe/meatball/meatball';
/**
 * Generated class for the SnackListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-snack-list',
  templateUrl: 'snack-list.html',
})
export class SnackListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SnackListPage');
  }
  goToPudding(){
    this.navCtrl.push(CornPuddingPage);
  }

  goToPuddingSnack(){
    this.navCtrl.push(ExtraPuddingPage);
  }
  goToPopCorn(){
    this.navCtrl.push(GarlicPopCornPage);
  }

  goToMealBall(){
    this.navCtrl.push(MeatballPage);
  }
}
