import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SnackListPage } from './snack-list';

@NgModule({
  declarations: [
    SnackListPage,
  ],
  imports: [
    IonicPageModule.forChild(SnackListPage),
  ],
})
export class SnackListPageModule {}
