import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApppointmentAddPage } from '../appointment-add/appointment-add';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the AppointmentList page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-appointment-list',
  templateUrl: 'appointment-list.html',
})
export class AppointmentListPage {
  list:Observable<any>;

  constructor(private appointmentprovider: AppointmentProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.list = this.appointmentprovider.fetchData();
      
    this.list
    .subscribe(data => {
      console.log('result: ', data);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentListPage');
  }

  openAppointmentAdd(){
    this.navCtrl.push(ApppointmentAddPage, {
      item: "New Appointment"
    });
    return false;
  }

}
