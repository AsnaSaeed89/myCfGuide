import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OmlettePage } from '../breakfast-recipie/omlette/omlette';
import { GranolaPage } from '../breakfast-recipie/granola/granola';
import { ToastPage } from '../breakfast-recipie/toast/toast';
import { BeanToastPage } from '../breakfast-recipie/bean-toast/bean-toast';


/**
 * Generated class for the BreakfastListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-breakfast-list',
  templateUrl: 'breakfast-list.html',
  entryComponents:[OmlettePage,GranolaPage,ToastPage,BeanToastPage]
})
export class BreakfastListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BreakfastListPage');
  }


  goToOmelette(){
    this.navCtrl.push(OmlettePage);
  }

  goToGranola(){
    this.navCtrl.push(GranolaPage);
  }
  goToToast(){
    this.navCtrl.push(ToastPage);
  }

  goToBeanToast(){
    this.navCtrl.push(BeanToastPage);
  }



  
}
