import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BreakfastListPage } from './breakfast-list';

@NgModule({
  declarations: [
    BreakfastListPage,
  ],
  imports: [
    IonicPageModule.forChild(BreakfastListPage),
  ],
})
export class BreakfastListPageModule {}
