import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HighProtienSmoothiePage } from './high-protien-smoothie';

@NgModule({
  declarations: [
    HighProtienSmoothiePage,
  ],
  imports: [
    IonicPageModule.forChild(HighProtienSmoothiePage),
  ],
})
export class HighProtienSmoothiePageModule {}
