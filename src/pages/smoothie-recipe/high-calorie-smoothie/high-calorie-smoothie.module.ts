import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HighCalorieSmoothiePage } from './high-calorie-smoothie';

@NgModule({
  declarations: [
    HighCalorieSmoothiePage,
  ],
  imports: [
    IonicPageModule.forChild(HighCalorieSmoothiePage),
  ],
})
export class HighCalorieSmoothiePageModule {}
