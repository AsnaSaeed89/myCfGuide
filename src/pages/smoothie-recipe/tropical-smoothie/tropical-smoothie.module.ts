import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TropicalSmoothiePage } from './tropical-smoothie';

@NgModule({
  declarations: [
    TropicalSmoothiePage,
  ],
  imports: [
    IonicPageModule.forChild(TropicalSmoothiePage),
  ],
})
export class TropicalSmoothiePageModule {}
