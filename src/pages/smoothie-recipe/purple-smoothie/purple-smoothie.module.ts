import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurpleSmoothiePage } from './purple-smoothie';

@NgModule({
  declarations: [
    PurpleSmoothiePage,
  ],
  imports: [
    IonicPageModule.forChild(PurpleSmoothiePage),
  ],
})
export class PurpleSmoothiePageModule {}
