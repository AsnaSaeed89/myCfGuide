import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DessertListPage } from './dessert-list';

@NgModule({
  declarations: [
    DessertListPage,
  ],
  imports: [
    IonicPageModule.forChild(DessertListPage),
  ],
})
export class DessertListPageModule {}
