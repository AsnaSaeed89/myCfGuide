import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FantasyFudgePage } from '../dessert-recipe/fantasy-fudge/fantasy-fudge';
import { CherryDhanishPage } from '../dessert-recipe/cherry-dhanish/cherry-dhanish';
import { IceCreamBreadPage } from '../dessert-recipe/ice-cream-bread/ice-cream-bread';
import { OreoBallPage } from '../dessert-recipe/oreo-ball/oreo-ball';

/**
 * Generated class for the DessertListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dessert-list',
  templateUrl: 'dessert-list.html',
})
export class DessertListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DessertListPage');
  }
  goToDhanisPastry(){
    this.navCtrl.push(CherryDhanishPage);
  }

  goToFudge(){
    this.navCtrl.push(FantasyFudgePage);
  }
  goToIceCreamBread(){
    this.navCtrl.push(IceCreamBreadPage);
  }

  goToOreoBalls(){
    this.navCtrl.push(OreoBallPage);
  }
}
