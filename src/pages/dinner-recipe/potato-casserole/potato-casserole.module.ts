import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PotatoCasserolePage } from './potato-casserole';

@NgModule({
  declarations: [
    PotatoCasserolePage,
  ],
  imports: [
    IonicPageModule.forChild(PotatoCasserolePage),
  ],
})
export class PotatoCasserolePageModule {}
