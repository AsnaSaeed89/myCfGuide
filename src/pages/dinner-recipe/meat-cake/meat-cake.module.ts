import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeatCakePage } from './meat-cake';

@NgModule({
  declarations: [
    MeatCakePage,
  ],
  imports: [
    IonicPageModule.forChild(MeatCakePage),
  ],
})
export class MeatCakePageModule {}
