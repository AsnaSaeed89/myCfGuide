import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TurkeySupremePage } from './turkey-supreme';

@NgModule({
  declarations: [
    TurkeySupremePage,
  ],
  imports: [
    IonicPageModule.forChild(TurkeySupremePage),
  ],
})
export class TurkeySupremePageModule {}
