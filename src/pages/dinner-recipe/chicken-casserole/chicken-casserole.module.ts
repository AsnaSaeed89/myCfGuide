import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChickenCasserolePage } from './chicken-casserole';

@NgModule({
  declarations: [
    ChickenCasserolePage,
  ],
  imports: [
    IonicPageModule.forChild(ChickenCasserolePage),
  ],
})
export class ChickenCasserolePageModule {}
