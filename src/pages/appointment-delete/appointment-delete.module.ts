import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentDeletePage } from './appointment-delete';

@NgModule({
  declarations: [
    AppointmentDeletePage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentDeletePage),
  ],
})
export class AppointmentDeletePageModule {}
