import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OreoBallPage } from './oreo-ball';

@NgModule({
  declarations: [
    OreoBallPage,
  ],
  imports: [
    IonicPageModule.forChild(OreoBallPage),
  ],
})
export class OreoBallPageModule {}
