import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FantasyFudgePage } from './fantasy-fudge';

@NgModule({
  declarations: [
    FantasyFudgePage,
  ],
  imports: [
    IonicPageModule.forChild(FantasyFudgePage),
  ],
})
export class FantasyFudgePageModule {}
