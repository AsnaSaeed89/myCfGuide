import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IceCreamBreadPage } from './ice-cream-bread';

@NgModule({
  declarations: [
    IceCreamBreadPage,
  ],
  imports: [
    IonicPageModule.forChild(IceCreamBreadPage),
  ],
})
export class IceCreamBreadPageModule {}
