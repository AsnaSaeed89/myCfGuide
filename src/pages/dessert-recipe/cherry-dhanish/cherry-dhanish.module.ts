import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CherryDhanishPage } from './cherry-dhanish';

@NgModule({
  declarations: [
    CherryDhanishPage,
  ],
  imports: [
    IonicPageModule.forChild(CherryDhanishPage),
  ],
})
export class CherryDhanishPageModule {}
