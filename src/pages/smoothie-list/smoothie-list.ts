import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HighCalorieSmoothiePage } from '../smoothie-recipe/high-calorie-smoothie/high-calorie-smoothie';
import { HighProtienSmoothiePage } from '../smoothie-recipe/high-protien-smoothie/high-protien-smoothie';
import { PurpleSmoothiePage } from '../smoothie-recipe/purple-smoothie/purple-smoothie';
import { TropicalSmoothiePage } from '../smoothie-recipe/tropical-smoothie/tropical-smoothie';

/**
 * Generated class for the SmoothieListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-smoothie-list',
  templateUrl: 'smoothie-list.html',
})
export class SmoothieListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SmoothieListPage');
  }
  goToTropical(){
    this.navCtrl.push(TropicalSmoothiePage);
  }

  goToHighCalorie(){
    this.navCtrl.push(HighCalorieSmoothiePage);
  }
  goToPurple(){
    this.navCtrl.push(PurpleSmoothiePage);
  }

  goToProtein(){
    this.navCtrl.push(HighProtienSmoothiePage);
  }
}
