import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmoothieListPage } from './smoothie-list';

@NgModule({
  declarations: [
    SmoothieListPage,
  ],
  imports: [
    IonicPageModule.forChild(SmoothieListPage),
  ],
})
export class SmoothieListPageModule {}
