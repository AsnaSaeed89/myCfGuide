import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GranolaPage } from './granola';

@NgModule({
  declarations: [
    GranolaPage,
  ],
  imports: [
    IonicPageModule.forChild(GranolaPage),
  ],
})
export class GranolaPageModule {}
