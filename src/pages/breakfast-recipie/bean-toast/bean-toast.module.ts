import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeanToastPage } from './bean-toast';

@NgModule({
  declarations: [
    BeanToastPage,
  ],
  imports: [
    IonicPageModule.forChild(BeanToastPage),
  ],
})
export class BeanToastPageModule {}
