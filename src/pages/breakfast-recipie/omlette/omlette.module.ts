import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OmlettePage } from './omlette';

@NgModule({
  declarations: [
    OmlettePage,
  ],
  imports: [
    IonicPageModule.forChild(OmlettePage),
  ],
})
export class OmlettePageModule {}
