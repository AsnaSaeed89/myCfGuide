import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AsianSaladPage } from '../lunch-recipe/asian-salad/asian-salad';
import { ButterSoupPage } from '../lunch-recipe/butter-soup/butter-soup';
import { CheeceLasagnePage } from  '../lunch-recipe/cheece-lasagne/cheece-lasagne';
import { SalminHerbPage } from  '../lunch-recipe/salmin-herb/salmin-herb';

/**
 * Generated class for the LunchListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lunch-list',
  templateUrl: 'lunch-list.html',
})
export class LunchListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LunchListPage');
  }
  goToSalad(){
    this.navCtrl.push(AsianSaladPage);
  }

  goToCheeseLasagna(){
    this.navCtrl.push(CheeceLasagnePage);
  }
  goToSoup(){
    this.navCtrl.push(ButterSoupPage);
  }

  goToSalmon(){
    this.navCtrl.push(SalminHerbPage);
  }

}
