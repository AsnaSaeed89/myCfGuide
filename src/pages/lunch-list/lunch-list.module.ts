import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LunchListPage } from './lunch-list';

@NgModule({
  declarations: [
    LunchListPage,
  ],
  imports: [
    IonicPageModule.forChild(LunchListPage),
  ],
})
export class LunchListPageModule {}
